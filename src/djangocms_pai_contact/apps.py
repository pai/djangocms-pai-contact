from django.apps import AppConfig


class DjangocmsPaiContactConfig(AppConfig):
    name = 'djangocms_pai_contact'
